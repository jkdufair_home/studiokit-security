﻿using NuGet;
using System;

namespace StudioKit.Security.Extensions
{
	public static class AppClientExtensions
	{
		public static SemanticVersion SemanticCurrentVersion(this AppClient appClient)
		{
			return appClient.CurrentVersion == null ? null : new SemanticVersion(appClient.CurrentVersion);
		}

		public static SemanticVersion SemanticMinVersion(this AppClient appClient)
		{
			return appClient.MinVersion == null ? null : new SemanticVersion(appClient.MinVersion);
		}

		public static bool IsVersionCurrent(this AppClient appClient, string version)
		{
			if (appClient.CurrentVersion == null)
				return true;
			if (version == null)
				throw new ArgumentNullException(nameof(version));

			SemanticVersion.TryParse(version, out SemanticVersion semanticVersion);
			if (semanticVersion == null)
				throw new Exception("version failed to Parse as SemanticVersion");

			return semanticVersion.Version.CompareTo(appClient.SemanticCurrentVersion().Version) >= 0;
		}

		public static bool IsVersionSupported(this AppClient appClient, string version)
		{
			if (appClient.MinVersion == null)
				return true;
			if (version == null)
				throw new ArgumentNullException(nameof(version));

			SemanticVersion.TryParse(version, out SemanticVersion semanticVersion);
			if (semanticVersion == null)
				throw new Exception("version failed to Parse as SemanticVersion");

			return semanticVersion.Version.CompareTo(appClient.SemanticMinVersion().Version) != -1;
		}
	}
}