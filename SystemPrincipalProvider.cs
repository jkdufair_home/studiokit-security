﻿using StudioKit.Security.Interfaces;
using System.Security.Principal;

namespace StudioKit.Security
{
	/// <summary>
	/// An <see cref="IPrincipalProvider"/> that returns a system principal <see cref="SystemPrincipal"/>.
	/// To be used when a user specifc principal is not available, and actions are being completed by the system itself.
	/// </summary>
	public class SystemPrincipalProvider : IPrincipalProvider
	{
		public IPrincipal GetPrincipal()
		{
			return new SystemPrincipal();
		}
	}
}