﻿namespace StudioKit.Security
{
	public static class AuthorizationGrantType
	{
		public static string Password = "password";
		public static string AuthorizationCode = "authorization_code";
		public static string RefreshToken = "refresh_token";
		public static string ClientCredentials = "client_credentials";
	}
}