﻿using System.ComponentModel.DataAnnotations;

namespace StudioKit.Security
{
	/// <summary>
	/// A client for use by an external service.
	/// </summary>
	public class ServiceClient : Client
	{
		public ServiceClient()
		{
			Grants = AuthorizationGrantType.ClientCredentials;
		}

		[Required]
		public string Name { get; set; }
	}
}