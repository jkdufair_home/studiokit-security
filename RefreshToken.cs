﻿using StudioKit.Data;
using System;

namespace StudioKit.Security
{
	public class RefreshToken : ModelBase
	{
		public string Token { get; set; }

		public byte[] AuthenticationTicket { get; set; }

		public DateTime AuthenticationTicketExpiration { get; set; }
	}
}