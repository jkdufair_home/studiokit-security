﻿using StudioKit.Data.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace StudioKit.Security
{
	/// <summary>
	/// An abstract base class for other types of clients.
	/// </summary>
	public abstract class Client : IAuditable
	{
		protected Client()
		{
			Secret = Guid.NewGuid().ToString();
		}

		[Key]
		[StringLength(100)]
		public string ClientId { get; set; }

		[Required]
		public string Secret { get; set; }

		[Required]
		[IgnoreDataMember]
		public string Grants { get; set; }

		[Timestamp]
		public byte[] RowVersion { get; set; }

		public DateTime DateStored { get; set; }

		public DateTime DateLastUpdated { get; set; }

		public string LastUpdatedById { get; set; }
	}
}