﻿using System.Security.Principal;

namespace StudioKit.Security.Interfaces
{
	public interface IPrincipalProvider
	{
		IPrincipal GetPrincipal();
	}
}