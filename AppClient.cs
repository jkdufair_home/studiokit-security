﻿namespace StudioKit.Security
{
	/// <summary>
	/// A client for use by an app, e.g. Web, iOS, Android.
	/// </summary>
	public class AppClient : Client
	{
		public AppClient()
		{
			Grants = string.Join(",", AuthorizationGrantType.AuthorizationCode, AuthorizationGrantType.RefreshToken);
		}

		public string CurrentVersion { get; set; }

		public string MinVersion { get; set; }
	}
}