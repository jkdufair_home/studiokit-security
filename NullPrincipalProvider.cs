﻿using StudioKit.Security.Interfaces;
using System.Security.Principal;

namespace StudioKit.Security
{
	/// <summary>
	/// An <see cref="IPrincipalProvider"/> that returns a null principal.
	/// To be used when a principal is not available, e.g. console apps, package manager console, etc.
	/// </summary>
	public class NullPrincipalProvider : IPrincipalProvider
	{
		public IPrincipal GetPrincipal()
		{
			return null;
		}
	}
}