﻿using System.Security.Principal;

namespace StudioKit.Security
{
	/// <summary>
	/// An <see cref="IPrincipal"/> representing the system itself.
	/// </summary>
	public class SystemPrincipal : IPrincipal
	{
		public SystemPrincipal()
		{
			Identity = new SystemIdentity();
		}

		public bool IsInRole(string role)
		{
			return true;
		}

		public IIdentity Identity { get; }
	}
}